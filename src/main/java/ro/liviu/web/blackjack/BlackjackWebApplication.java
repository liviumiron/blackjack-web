package ro.liviu.web.blackjack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"ro.liviu.blackjack"})
public class BlackjackWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlackjackWebApplication.class, args);
	}
}
