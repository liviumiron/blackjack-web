package ro.liviu.blackjack.transport;

import java.util.List;

import ro.liviu.blackjack.interfaces.Card;

public class GameState {
	private List<Card> playerHand;
	private List<Card> dealerHand;
	
	private int playerScore;
	private int dealerScore;
	
	private String state;
	
	public List<Card> getPlayerHand() {
		return playerHand;
	}
	public void setPlayerHand(List<Card> playerHand) {
		this.playerHand = playerHand;
	}
	public List<Card> getDealerHand() {
		return dealerHand;
	}
	public void setDealerHand(List<Card> dealerHand) {
		this.dealerHand = dealerHand;
	}
	public int getPlayerScore() {
		return playerScore;
	}
	public void setPlayerScore(int playerScore) {
		this.playerScore = playerScore;
	}
	public int getDealerScore() {
		return dealerScore;
	}
	public void setDealerScore(int dealerScore) {
		this.dealerScore = dealerScore;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	
	
}
