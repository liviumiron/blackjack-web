package ro.liviu.blackjack.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.liviu.blackjack.entities.WebGame;
import ro.liviu.blackjack.interfaces.Game;
import ro.liviu.blackjack.interfaces.GameSessionHandler;
import ro.liviu.blackjack.transport.GameState;

@RestController
public class GameController {
	
	
	@Autowired
	GameSessionHandler gameHandler;
	
	@RequestMapping(value = "/game/new/{id}", method = RequestMethod.POST)
	public GameState newGame(@PathVariable String id) {
		WebGame webGame = (WebGame)gameHandler.newGame(id);
		GameState state = new GameState();
		state.setDealerHand(webGame.getDealerHand());
		state.setPlayerHand(webGame.getPlayerHand());
		state.setDealerScore(webGame.getDealerScore());
		state.setPlayerScore(webGame.getPlayerScore());
		
		state.setState(webGame.handleWebEndgame(webGame.checkEndgame(false, false)));
		return state;
	}
	
	
	
	@RequestMapping(value = "/game/move/{id}/action/{command}", method = RequestMethod.GET)
	public GameState moveGame(@PathVariable String id, @PathVariable String command) {
		WebGame webGame = (WebGame)gameHandler.getGame(id);
		String res = "";
		if (command.equals("y")) {
			res = webGame.playerTurn(command);
		} else {
			res = webGame.dealerTurnWeb();
		}
		GameState state = new GameState();
		state.setDealerHand(webGame.getDealerHand());
		state.setPlayerHand(webGame.getPlayerHand());
		state.setDealerScore(webGame.getDealerScore());
		state.setPlayerScore(webGame.getPlayerScore());
		state.setState(res);
		return state;
	}
}
