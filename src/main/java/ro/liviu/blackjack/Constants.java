package ro.liviu.blackjack;

public class Constants {
	public static final int TWENTYONE = 21;
	public static final String ACE = "Ace";
	public static final String LOWCARD = "LowCard";
	public static final String HIGHCARD = "HighCard";
	public static final int DEALER_LIMIT = 17;
	
}
