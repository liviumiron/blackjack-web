package ro.liviu.blackjack.entities;

import ro.liviu.blackjack.enums.Endgame;
import ro.liviu.blackjack.interfaces.Deck;
import ro.liviu.blackjack.interfaces.Game;
import ro.liviu.blackjack.interfaces.Hand;

import static ro.liviu.blackjack.Constants.TWENTYONE;
import static ro.liviu.blackjack.Constants.DEALER_LIMIT;


import java.util.Scanner;
/**
 * A console blackjack game. The user can interact by entering y or n if it want's 
 * to get hit with another card(it's case insensitive).
 * 
 * @author liviu
 *
 */
public class ConsoleGame implements Game {
	
	
	private Deck deck;
	private Hand player;
	private Hand dealer;
	
	private boolean stop;
	
	private Scanner input;
	
	public ConsoleGame() {
		initialize();
		
		input = new Scanner(System.in);
		
		
		run();
	}

	/**
	 * Creates the deck and the hands
	 */
	@Override
	public void initialize() {
		
		deck = new DeckImpl();
		
		player = new HandImpl("Player");
		dealer = new HandImpl("Dealer");
		
	}
	
	/**
	 * Asks the player if he want's another card and hits him with a card if he chose yes
	 */
	@Override
	public void playerTurn() {
		System.out.println("Do you want to draw another hand? (Y/N)");
		String answer = null;
		
		while ((answer = input.next()).equalsIgnoreCase("y")) {
			
			player.addCard(deck.getCard());
			player.printHand();
			handleEndgame(checkEndgame(false, false));
			if (player.getValue() != TWENTYONE) {
				System.out.println("Do you want to draw another hand? (Y/N)");
			} else {
				break;
			}
			
		}
		
		
	}
	
	/**
	 * Shows the dealer's hidden hand and continues to play until it hits the limit
	 */
	@Override
	public void dealerTurn() {
		System.out.println("Dealer's hidden card was: " + dealer.getHiddenHand().get().getName());
		
		dealer.showHiddenHand();
		
		dealer.printHand();
		if (dealer.getValue() == TWENTYONE) {
			handleEndgame(checkEndgame(true, true));
		}
		
		while (dealer.getValue() < DEALER_LIMIT) {
			System.out.println("Dealer draws another hand");
			dealer.addCard(deck.getCard());
			dealer.printHand();
			handleEndgame(checkEndgame(true, false));
			
		}
		System.out.println("Dealer stops");
		handleEndgame(checkEndgame(true, true));
	}

	
	private void displayHand(Hand hand) {
		hand.printHand();
	}

	
	/**
	 * Checks if the game is at it's end
	 */
	@Override
	public Endgame checkEndgame(boolean dealerTurn, boolean endRequested) {
		int dealerScore = dealer.getValue();
		int playerScore = player.getValue();
		
		if (dealerTurn) {
			if (dealerScore > TWENTYONE) {
				return Endgame.VICTORY;
			}
		}
		
		if (!dealerTurn) {
			if (playerScore > TWENTYONE) {
				return Endgame.DEFEAT;
			}
		}
		
		if (endRequested) {
			if (dealerScore > playerScore ) {
				return Endgame.DEFEAT;
			} else if (dealerScore < playerScore) {
				return Endgame.VICTORY;
			} else {
				return Endgame.DRAW;
			}
		
		}
		return Endgame.CONTINUE;
	}

	

	/**
	 * Handles the end of the game
	 */
	@Override
	public void handleEndgame(Endgame endgame) {
		if (endgame.equals(Endgame.CONTINUE)) {
			return;
		}
		
		if (endgame.equals(Endgame.DEFEAT)) {
			System.out.println("Dealer wins!");
			System.exit(0);
		} else if (endgame.equals(Endgame.VICTORY)) {
			System.out.println("Player wins!");
			System.exit(0);
		} else {
			System.out.println("Draw!");
			System.exit(0);
		}
		
	}

	/**
	 * Runs the game
	 */
	public void run() {
		dealer.addCard(deck.getCard());
		dealer.addCard(deck.getCard(true));
		player.addCard(deck.getCard());
		player.addCard(deck.getCard());
		
		
		System.out.println("Initial draw");
		displayHand(dealer);
		displayHand(player);
		//check for blackjack
		handleEndgame(checkEndgame(false, false));
		
		
		playerTurn();
		
		dealerTurn();
		
	}

	

}
