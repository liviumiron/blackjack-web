package ro.liviu.blackjack.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ro.liviu.blackjack.interfaces.Game;
import ro.liviu.blackjack.interfaces.GameSessionHandler;

@Component
public class GameHandler implements GameSessionHandler {

	private Map<String, Game> gameMap;
	
	
	public GameHandler() {
		gameMap = new HashMap<String, Game>();
	}

	
	@Override
	public Game getGame(String id) {
		return gameMap.get(id);
	}

	@Override
	public Game newGame(String id) {
		Game game = new WebGame();
		gameMap.put(id, game);
		return gameMap.get(id);
	}

	@Override
	public List<String> getIds() {
		List<String> allIds = new ArrayList<String>();
		allIds.addAll(gameMap.keySet());
		return allIds;
	}


	@Override
	public void removeGame(String id) {
		gameMap.remove(id);
		
	}

}
