package ro.liviu.blackjack.entities;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import ro.liviu.blackjack.entities.LowCard.Type;
import ro.liviu.blackjack.enums.Suite;
import ro.liviu.blackjack.interfaces.Card;
import ro.liviu.blackjack.interfaces.Deck;

public class DeckImpl implements Deck {
	
	private static final long DECK_SEED = 42;
	private Stack<Card> cards;

	public DeckImpl() {
		initialize();
		
	}
	
	
	/**
	 * Creates all the cards in the game
	 */
	@Override
	public void initialize() {
		cards = new Stack<Card>();
		
		createLowCards();
		
		
		createHighCards();
		
		createAces();
		
		shuffle();
	}

	private void createLowCards() {
		for (int i = 0; i < 8; i++) {
			cards.push(new LowCard(LowCard.Type.values()[i], Suite.HEARTS));
			cards.push(new LowCard(LowCard.Type.values()[i], Suite.DIAMONDS));
			cards.push(new LowCard(LowCard.Type.values()[i], Suite.SPADES));
			cards.push(new LowCard(LowCard.Type.values()[i], Suite.CLUBS));
		}
	}
	
	private void createHighCards() {
		createHighCard(HighCard.Type.TEN);
		createHighCard(HighCard.Type.J);
		createHighCard(HighCard.Type.Q);
		createHighCard(HighCard.Type.K);
	}
	
	private void createHighCard(HighCard.Type type) {
		cards.push(new HighCard(type, Suite.HEARTS));
		cards.push(new HighCard(type, Suite.DIAMONDS));
		cards.push(new HighCard(type, Suite.SPADES));
		cards.push(new HighCard(type, Suite.CLUBS));
	}
	
	private void createAces() {
		cards.push(new AceCard(Suite.HEARTS));
		cards.push(new AceCard(Suite.DIAMONDS));
		cards.push(new AceCard(Suite.SPADES));
		cards.push(new AceCard(Suite.CLUBS));
		
	}

	@Override
	public Card getCard(boolean faceDown) {
		Card c = cards.pop();
		if (faceDown) {
			c.setFaceUp(false);
		}
		return c;
	}
	
	/**
	 * Removes a card from the deck
	 */
	@Override
	public Card getCard() {
		Card c = cards.pop();
		c.setFaceUp(true);
		return c;
	}

	/**
	 * Prints all the cards in the deck
	 */
	@Override
	public void printDeck() {
		for (Card c : cards) {
			System.out.println(c.getName());
		}
		
	}

	/**
	 * Shuffles all the cards in the deck
	 */
	@Override
	public void shuffle() {
		LocalDateTime now = LocalDateTime.now();
		Random random = new Random(now.toEpochSecond(ZoneOffset.UTC) + now.getNano() + DECK_SEED);
		
		Collections.shuffle(cards, random);
		
	}
	
	

}
