package ro.liviu.blackjack.entities;

import static ro.liviu.blackjack.Constants.DEALER_LIMIT;
import static ro.liviu.blackjack.Constants.TWENTYONE;

import java.util.List;
import java.util.Scanner;

import org.springframework.stereotype.Component;

import ro.liviu.blackjack.enums.Endgame;
import ro.liviu.blackjack.interfaces.Card;
import ro.liviu.blackjack.interfaces.Deck;
import ro.liviu.blackjack.interfaces.Game;
import ro.liviu.blackjack.interfaces.Hand;


public class WebGame implements Game {
	
	private Deck deck;
	private Hand player;
	private Hand dealer;
	
	private String state;
	
	boolean hasHidden;
	
	public WebGame() {
		initialize();
		
		hasHidden = true;
		
	}

	@Override
	public void initialize() {
		deck = new DeckImpl();
		
		player = new HandImpl("Player");
		dealer = new HandImpl("Dealer");
		
		
		dealer.addCard(deck.getCard());
		dealer.addCard(deck.getCard(true));
		player.addCard(deck.getCard());
		player.addCard(deck.getCard());
		
		
		
		
		
	}

	public List<Card> getHand(Hand hand) {
		return hand.getHand();
	}
	
	public List<Card> getPlayerHand() {
		return player.getHand();
	}
	
	public List<Card> getDealerHand() {
		return dealer.getHand();
	}
	
	public int getPlayerScore() {
		return player.getValue();
	}
	
	public int getDealerScore() {
		return dealer.getValue();
	}
	
	
	@Override
	public void playerTurn() {
		
		
		
		
	}
	
	public String playerTurn(String action) {
		
		
	 		player.addCard(deck.getCard());
			player.printHand();
			
			
			return handleWebEndgame(checkEndgame(false, false));
		
		
		
	}

	@Override
	public void dealerTurn() {
		
		
	}
	
	
	public String dealerTurnWeb() {
		if (hasHidden) {
			dealer.showHiddenHand();
			
		}
		
		
		if (dealer.getValue() < DEALER_LIMIT) {
			dealer.addCard(deck.getCard());
			Endgame eg = checkEndgame(true, false);
			if (!eg.equals(Endgame.CONTINUE)) {
				return handleWebEndgame(eg);
			} else {
				return Endgame.CONTINUE.getName().toLowerCase();
			}
		} else {
			 System.out.println("Over dealer limit");
			 return handleWebEndgame(checkEndgame(true, true));
		}
	
		
	}
	
	

	@Override
	public Endgame checkEndgame(boolean dealerTurn, boolean endRequested) {
		int dealerScore = dealer.getValue();
		int playerScore = player.getValue();
		
		if (dealerTurn) {
			if (dealerScore > TWENTYONE) {
				return Endgame.VICTORY;
			}
		}
		
		if (!dealerTurn) {
			if (playerScore > TWENTYONE) {
				return Endgame.DEFEAT;
			}
		}
		
		if (endRequested) {
			if (dealerScore > playerScore ) {
				return Endgame.DEFEAT;
			} else if (dealerScore < playerScore) {
				return Endgame.VICTORY;
			} else {
				return Endgame.DRAW;
			}
		
		}
		return Endgame.CONTINUE;
	}

	@Override
	public void handleEndgame(Endgame endgame) {
		// TODO Auto-generated method stub
		
	}
	
	
	public String handleWebEndgame(Endgame endgame) {
		if (endgame.equals(Endgame.CONTINUE)) {
			return Endgame.CONTINUE.getName().toLowerCase();
		}
		
		if (endgame.equals(Endgame.DEFEAT)) {
			return Endgame.DEFEAT.getName().toLowerCase();
		} else if (endgame.equals(Endgame.VICTORY)) {
			return Endgame.VICTORY.getName().toLowerCase();
		} else {
			return Endgame.DRAW.getName().toLowerCase();
		}
		
	}


}
