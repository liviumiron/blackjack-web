package ro.liviu.blackjack.entities;

import ro.liviu.blackjack.enums.Suite;
import ro.liviu.blackjack.interfaces.Card;
import static ro.liviu.blackjack.Constants.LOWCARD;
/**
 * All the cards between 2 and 9
 * 
 * @author liviu
 *
 */

public class LowCard implements Card {
	
	public static String CARDTYPE = LOWCARD;
	
	public enum Type {
		TWO(2),
		THREE(3),
		FOUR(4),
		FIVE(5),
		SIX(6),
		SEVEN(7),
		EIGHT(8),
		NINE(9);
		
		private final int value;
	    private Type(int value) {
	        this.value = value;
	    }

	    public int getValue() {
	        return value;
	    }
	}
	
	private Type type;
	private Suite suite;
	private boolean faceUp;

	public LowCard(Type type, Suite suite) {
		this(type, suite, false);
	}
	
	public LowCard(Type type, Suite suite, boolean faceUp) {
		this.type = type;
		this.suite = suite;
		this.faceUp = faceUp;
	}
	
	@Override
	public int getValue() {
		return type.getValue();
	}

	@Override
	public boolean isFaceUp() {
		return this.faceUp;
	}

	@Override
	public String getName() {
		return type.getValue() + " " + suite.getName();
	}

	@Override
	public void setFaceUp(boolean faceUp) {
		this.faceUp = faceUp;
	}

	@Override
	public String getSuite() {
		return this.suite.getName().toLowerCase();
	}

	@Override
	public String getRank() {
		
		return this.type.getValue()+"";
	}

}
