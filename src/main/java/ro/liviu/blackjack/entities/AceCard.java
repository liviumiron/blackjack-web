package ro.liviu.blackjack.entities;

import ro.liviu.blackjack.entities.HighCard.Type;
import ro.liviu.blackjack.enums.Suite;
import ro.liviu.blackjack.interfaces.Card;
import static ro.liviu.blackjack.Constants.ACE;

/**
 * The Ace card. It's value can either be 1 or 11.
 */
public class AceCard implements Card {
	
	public static final String CARDTYPE = ACE;
	
	public static final int LOWVALUE = 1;
	public static final int HIGHVALUE = 11;
	
	
	private Suite suite;
	private boolean faceUp;
	private boolean valueOne;
	
	
	public AceCard(Suite suite) {
		this(suite, false);
	}
	
	public AceCard(Suite suite, boolean faceUp) {
		this.suite = suite;
		this.faceUp = faceUp;
		this.valueOne = false;
	}

	/**
	 * Depending on the field valueOne will return 1 or 11
	 */
	@Override
	public int getValue() {
		return this.valueOne ? LOWVALUE : HIGHVALUE;
	}
	
	/**
	 * Sets valueOne to true
	 */
	public void setValueOne() {
		this.valueOne = true;
	}

	@Override
	public boolean isFaceUp() {
		return this.faceUp;
	}

	@Override
	public void setFaceUp(boolean faceUp) {
		this.faceUp = faceUp;
		
	}

	@Override
	public String getName() {
		return CARDTYPE + " " + suite.getName();
	}

	@Override
	public String getSuite() {
		
		return this.suite.getName().toLowerCase();
	}

	@Override
	public String getRank() {
		// TODO Auto-generated method stub
		return "A";
	}
	
	

}
