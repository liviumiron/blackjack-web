package ro.liviu.blackjack.entities;

import ro.liviu.blackjack.entities.LowCard.Type;
import ro.liviu.blackjack.enums.Suite;
import ro.liviu.blackjack.interfaces.Card;
import static ro.liviu.blackjack.Constants.HIGHCARD;
/**
 * All the cards between 10 and K
 * 
 * @author liviu
 *
 */

public class HighCard implements Card{
	public static String CARDTYPE = HIGHCARD;
	
	public enum Type {
		TEN("10"),
		J("J"),
		Q("Q"),
		K("K");
		
		private final String name;
	    private Type(String name) {
	        this.name = name;
	    }

	    public String getName() {
	        return this.name;
	    }
	}
	
	public static final int HIGHVALUE = 10;
	
	private Type type;
	private Suite suite;
	private boolean faceUp;
	
	public HighCard(Type type, Suite suite) {
		this(type, suite, false);
	}
	
	public HighCard(Type type, Suite suite, boolean faceUp) {
		this.type = type;
		this.suite = suite;
		this.faceUp = faceUp;
	}

	@Override
	public int getValue() {
		return HIGHVALUE;
	}

	@Override
	public boolean isFaceUp() {
		return this.faceUp;
	}

	@Override
	public String getName() {
		return type.getName() + " " + suite.getName();
	}

	@Override
	public void setFaceUp(boolean faceUp) {
		this.faceUp = faceUp;
	}

	@Override
	public String getSuite() {
		return suite.getName().toLowerCase();
	}

	@Override
	public String getRank() {
		return type.name;
	}

}
