package ro.liviu.blackjack.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import ro.liviu.blackjack.interfaces.Card;
import ro.liviu.blackjack.interfaces.Hand;

import static ro.liviu.blackjack.Constants.TWENTYONE;
import static ro.liviu.blackjack.Constants.ACE;

/**
 * A hand that can hold cards
 * @author liviu
 *
 */
public class HandImpl implements Hand {
	
	private List<Card> cards;
	private String name;
	private int draw;
	
	public HandImpl(String name) {
		cards = new ArrayList<Card>();
		this.name = name;
		this.draw = -1;
	}

	@Override
	public int getValue() {
		int value = 0;
		
		value = cards.stream().mapToInt(Card::getValue).sum();
		
		
		/*
		 * If the value is over 21 we change the values of the Aces to one until we are below 21 or we have no more aces
		 */
		synchronized (cards){
			if (value > TWENTYONE) {
				List<Card> aces = cards.stream().filter(c -> {
					
					return c.getName().startsWith(ACE);
				}).collect(Collectors.toList());
				
				if (aces.size() > 0) {
					AceCard ace = null;
					while ((value = cards.stream().mapToInt(Card::getValue).sum()) > TWENTYONE && aces.size() > 0) {
						
						for (Card c : cards) {
						
							if (c.getName().startsWith(ACE)) {
								ace = (AceCard)c;
								if (ace.getValue() == AceCard.HIGHVALUE) {
									ace.setValueOne();
									aces.remove(0);
									break;
								}
							};
						
						}
						
					}
					
				
				}
				
			}
		}
		
		return value;
	}

	@Override
	public void addCard(Card card) {
		draw++;
		cards.add(card);
		
	}
	

	/**
	 * Prints the hand of a player
	 */
	@Override
	public void printHand() {
		System.out.print(this.name + " Draw("+draw+"): ");
		cards.stream().forEach( c -> { 
			if (c.isFaceUp()) {
				System.out.print(" " + c.getName());
			} else {
				System.out.print(" Hidden");
			}
			
		});
		System.out.println(". Total: " + getValue());
		System.out.println();
		
	}

	@Override
	public List<Card> getHand() {
		return this.cards;
	}

	/**
	 * Returns the dealer's hidden hand
	 */
	@Override
	public Optional<Card> getHiddenHand() {
		return cards.stream().filter(c -> !c.isFaceUp()).findFirst();
	}

	/**
	 * Makes the dealer's hidden hand visible
	 */
	@Override
	public void showHiddenHand() {
		for (Card c : cards) {
			if (!c.isFaceUp()) {
				c.setFaceUp(true);
				return;
			}
		}
		
	}

}
