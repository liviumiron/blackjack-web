package ro.liviu.blackjack.interfaces;

import java.util.List;

public interface GameSessionHandler {
	Game getGame(String id);
	Game newGame(String id);
	List<String> getIds();
	void removeGame(String id);
}
