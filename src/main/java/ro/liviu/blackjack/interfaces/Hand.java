package ro.liviu.blackjack.interfaces;

import java.util.List;
import java.util.Optional;

public interface Hand {
	
	public int getValue();
	
	public void addCard(Card card);
	
	public void printHand();
	
	public Optional<Card> getHiddenHand();
	
	public void showHiddenHand();
	
	public List<Card> getHand();

}
