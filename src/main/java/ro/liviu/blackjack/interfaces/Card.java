package ro.liviu.blackjack.interfaces;

public interface Card {
	
	String CARDTYPE = "Card";
	
	int getValue();
	
	boolean isFaceUp();
	
	void setFaceUp(boolean faceUp);
	
	String getName();
	
	String getSuite();
	
	String getRank();

}
