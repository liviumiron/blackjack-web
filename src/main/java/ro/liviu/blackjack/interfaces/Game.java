package ro.liviu.blackjack.interfaces;

import ro.liviu.blackjack.enums.Endgame;

public interface Game {
	
	void initialize();
	
	
	void playerTurn();
	
	void dealerTurn();
	
	Endgame checkEndgame(boolean dealerTurn, boolean endRequested);
	
	void handleEndgame(Endgame endgame);
	
	
	

}
