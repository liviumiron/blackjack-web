package ro.liviu.blackjack.enums;

public enum Suite {
	HEARTS("Hearts"),
	SPADES("Spades"),
	CLUBS("Clubs"),
	DIAMONDS("Diamonds");
	
	
	private final String name;
    private Suite(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
	
}
