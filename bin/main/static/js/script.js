var playerCards = [];
var dealerCards = [];
var id = new Date().getTime();

var lastState = "";


var interval = null;
function init() {
	newGame();
}

function newGame() {
	$.post( "/game/new/"+ id, function( data ) {
		$("#playerArea").empty();
		$("#dealerArea").empty();
		for (var i = 0; i < data.playerHand.length; i++) {
			draw(data.playerHand[i], true);
		}
		
		
		for (var i = 0; i < data.dealerHand.length; i++) {
			draw(data.dealerHand[i], false);
		}
		
		lastState = data.state;
		console.log(lastState);
		
		if (lastState == 'defeat') {
			alert('defeat');
			resetGame();
		}
		
		if (lastState == 'victory') {
			alert('victory');
			resetGame();
		}
		
		if (lastState == 'draw') {
			alert('draw');
			resetGame();
		}
		
		
	});
}

function resetGame() {
	$("#playerArea").empty();
	$("#dealerArea").empty();
}

function hit() {
	doSomethingServer("y");
}

function stop() {
	doSomethingServer("n");
	
	
	interval = setInterval(function() {
		doSomethingServer("n");
	}, 1000);
}

function doSomethingServer(action) {
	$.get( "/game/move/"+ id + "/action/" + action, function( data ) {
		$("#playerArea").empty();
		$("#dealerArea").empty();
		
		for (var i = 0; i < data.playerHand.length; i++) {
			draw(data.playerHand[i], true);
		}
		
		
		for (var i = 0; i < data.dealerHand.length; i++) {
			draw(data.dealerHand[i], false);
		}
		
		
		lastState = data.state;
		console.log(lastState);
		
		if (lastState == 'defeat') {
			clearInterval(interval);
			alert('defeat');
			resetGame();
		}
		
		if (lastState == 'victory') {
			clearInterval(interval);
			alert('victory');
			resetGame();
		}
		
		if (lastState == 'draw') {
			clearInterval(interval);
			alert('draw');
			resetGame();
		}
		
		
	});
}

function draw(card, isPlayer) {
	
	
	
	var suite = card.suite;
	if (suite == 'diamonds') {
		suite = 'diams';
	}
	var rank = card.rank;
	var Rank = rank.toLowerCase();
	
	
	
	var html;
	if (card.faceUp) {
		html = `<div class="card rank-${Rank} ${suite}"><span class="rank">${rank}</span><span class="suit">&${suite};</span>`;
	} else {
		html = `<div class="card back"></div>`;
	}
	console.log(html);
	if (isPlayer) {
		$("#playerArea").append($(html));
	} else {
		$("#dealerArea").append($(html));
	}
}

init();